import '../styles/main.scss'

// ==========================================================
let cardtypes = ['A', 'B', 'C', 'D', 'E', 'F']
let cards = [...cardtypes, ...cardtypes]
let memoryboard = document.getElementById('memorygame')
let comparearray = []
let cardelements = []
let matched = false
let remainingtiles = 0
let restartgame = document.querySelector('.restartgame')

// ==========================================================
const randomizeArray = (arr) => {
  for (let i = arr.length-1; i >=0; i--) {
   
      var randomIndex = Math.floor(Math.random()*(i+1)); 
      var itemAtIndex = arr[randomIndex]; 
       
      arr[randomIndex] = arr[i]; 
      arr[i] = itemAtIndex;
  }
  return arr;
}

// ==========================================================
function playSound(soundPath) {
  let sound = new Audio()
  sound.src = `sounds/${soundPath}`
  sound.play()
}

// ==========================================================
function clicked(e) {
  playSound("chord.wav")

  let parentElement = e.target.closest('.card')

  if (
      parentElement.classList.contains('matched') || 
      parentElement.classList.contains('preventClick')) {
    e.preventDefault()
    return
  }

  parentElement.classList.add('clicked')
  comparearray = [...comparearray, parentElement.getAttribute('data-value')]

  if (comparearray.length === 2) {
    for (let i=0; i<cardelements.length; i++) {
      cardelements[i].classList.add('preventClick')
    }

    // just for animation purposes
    setTimeout(() => {
      checkEquals()
    }, 1000)
  }
}

// ==========================================================
function checkEquals() {
  let clickedElements = document.querySelectorAll('.clicked')


  if (comparearray[0] === comparearray[1]) {
    for (let i=0; i<clickedElements.length; i++) {
      clickedElements[i].classList.add('matched')
    }

    matched = true
    remainingtiles -= 2
    playSound("tada.wav")
  } else {
    playSound("chimes.wav")
  }

  for (let i=0; i<clickedElements.length; i++) {
    clickedElements[i].classList.remove('clicked')
  }

  for (let x=0; x<cardelements.length; x++) {
    cardelements[x].classList.remove('preventClick')
  }

  comparearray = []

  matched = false

  if (remainingtiles === 0) {
    alert('You won the game!')

    restartgame.classList.add('show')
  }

}

function startGame() {
  // empty first if full (when restarting game)
  if (memoryboard.children.length > 0) {
    memoryboard.innerHTML = ""
  }

  randomizeArray(cards)

  for (let i=0; i<cards.length; i++) {
    let cardImage = new Image()
    cardImage.src = 'images/card.jpg'

    let createdDiv = document.createElement('div')
    createdDiv.classList.add('card')

    let createInnerDiv = document.createElement('div')
    createInnerDiv.classList.add('card-inner')

    let createFront = document.createElement('div')
    createFront.classList.add('front-card')

    let createBack = document.createElement('div')
    createBack.classList.add('back-card')

    createInnerDiv.appendChild(createFront)
    createInnerDiv.appendChild(createBack)

    createFront.appendChild(cardImage)
    createBack.innerText = cards[i]

    createdDiv.appendChild(createInnerDiv)
    createdDiv.setAttribute('data-value', cards[i])
    createFront.addEventListener('click', clicked, false)
    memoryboard.appendChild(createdDiv)
  }

  cardelements = document.querySelectorAll('.card')

  remainingtiles = cardelements.length
  
}
// ==========================================================
function restart() {
  restartgame.classList.remove('show')
  startGame()
}

// ==========================================================
window.onload = () => {
  startGame()
}

restartgame.addEventListener('click', restart)
